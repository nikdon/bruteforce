val catsVersion  = "0.9.0"
val circeVersion = "0.8.0"
val flinkVersion = "1.3.1"

val slf4jVersion        = "1.7.21"
val logBackVersion      = "1.1.7"
val scalaLoggingVersion = "3.5.0"

val slf4jApi       = "org.slf4j" % "slf4j-api" % slf4jVersion
val logBackClassic = "ch.qos.logback" % "logback-classic" % logBackVersion
val scalaLogging   = "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
val loggingStack   = Seq(slf4jApi, logBackClassic, scalaLogging)

val scalatest        = "org.scalatest" %% "scalatest" % "3.0.1" % "test"
val unitTestingStack = Seq(scalatest)

val commonDependencies = unitTestingStack ++ loggingStack

lazy val commonSettings = Seq(
  name := "bruteforce",
  version := "0.1.0",
  scalaVersion := "2.11.9",
  crossVersion := CrossVersion.binary,
  scalacOptions ++= Seq("-unchecked", "-deprecation"),
  libraryDependencies ++= commonDependencies
)

val cats = "org.typelevel" %% "cats" % catsVersion

val circeCore    = "io.circe" %% "circe-core" % circeVersion
val circeGeneric = "io.circe" %% "circe-generic-extras" % circeVersion
val circeParser  = "io.circe" %% "circe-parser" % circeVersion
val circeStack   = Seq(circeCore, circeGeneric, circeParser)

val flinkStack = Seq(
  "org.apache.flink" %% "flink-scala"           % flinkVersion,
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion
)

lazy val bruteforce = (project in file("."))
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Seq(cats) ++ flinkStack ++ loggingStack ++ circeStack ++ unitTestingStack,
    fork := true,
    addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full),
    assemblyJarName in assembly := "bruteforce.jar"
  )
