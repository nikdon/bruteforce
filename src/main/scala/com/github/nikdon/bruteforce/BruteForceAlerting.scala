package com.github.nikdon.bruteforce

import java.time.Instant

import com.github.nikdon.bruteforce.domain.{Alert, Event, Status}
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import cats.syntax.either._

import scala.util.Try

object BruteForceAlerting {

  final case class Params(
      inputHost: String,
      inputPort: Int,
      outputHost: String,
      outputPort: Int,
      threshold: Int,
      windowSize: Int, // seconds
      windowSlide: Int, // seconds
      isLocal: Boolean
  )

  object Params {
    def from(parameterTool: ParameterTool): Params = {
      Try(
        Params(
          parameterTool.get("inputHost"),
          parameterTool.getInt("inputPort"),
          parameterTool.get("outputHost"),
          parameterTool.getInt("outputPort"),
          parameterTool.getInt("threshold"),
          parameterTool.getInt("windowSize"),
          parameterTool.getInt("windowSlide"),
          parameterTool.getBoolean("local")
        )).getOrElse {
        System.err.println(
          "To start a bruteforce alerting system run with arguments:\n" +
            "--inputHost <inputHost> --inputPort <inputPort> " +
            "--outputHost <outputHost> --outputPort <outputPort> " +
            "--threshold <threshold> " +
            "--windowSize <windowSize> --windowSlide <windowSlide> " +
            "--local <local>"
        )
        throw new IllegalArgumentException()
      }
    }
  }

  // --inputHost localhost --inputPort 25501 --outputHost localhost --outputPort 25502 --threshold 3 --windowSize 10 --windowSlide 1 --local true
  def main(args: Array[String]): Unit = {
    val params = Params.from(ParameterTool.fromArgs(args))

    val env: StreamExecutionEnvironment = if (params.isLocal) {
      StreamExecutionEnvironment.createLocalEnvironment()
    } else {
      StreamExecutionEnvironment.getExecutionEnvironment
    }
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val eventStream = env.socketTextStream(params.inputHost, params.inputPort, '\n')

    val eventCounts =
      eventStream
        .flatMap(s => io.circe.parser.decode[Event.Login](s).toOption)
        .assignTimestampsAndWatermarks(Event.Login.EventTimestampExtractor)
        .map((_, 1))
        .keyBy(_._1.id)
        .window(SlidingEventTimeWindows.of(Time.seconds(params.windowSize), Time.seconds(params.windowSlide)))
        .sum(position = 1)

    eventCounts
      .filter(t => {
        val (event, count) = t
        val status = event.status match {
          case Status.Ok     => false
          case Status.Failed => true
        }
        val isAboveThreshold = count > params.threshold
        status && isAboveThreshold
      })
      .map(e => Alert.Login(e._1.id, e._2, Instant.now.toEpochMilli))
      .writeToSocket(params.outputHost, params.outputPort, new Alert.AlertSerializationSchema[Alert.Login])

    env.execute("Bruteforce Alerting")
  }
}
