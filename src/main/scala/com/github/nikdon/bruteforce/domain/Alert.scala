package com.github.nikdon.bruteforce.domain

import io.circe.Encoder
import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec, JsonKey}
import io.circe.syntax._
import org.apache.flink.streaming.util.serialization.SerializationSchema

@ConfiguredJsonCodec
sealed trait Alert
object Alert {
  implicit val config: Configuration = Configuration.default.withSnakeCaseKeys.withDefaults.withDiscriminator("_type")

  @ConfiguredJsonCodec
  case class Login(
      @JsonKey("_id") id: String,
      count: Int,
      creationDate: Long
  ) extends Alert

  class AlertSerializationSchema[A <: Alert: Encoder] extends SerializationSchema[A] {
    override def serialize(element: A): Array[Byte] = s"${(element: Alert).asJson.noSpaces}\n".getBytes
  }
}
