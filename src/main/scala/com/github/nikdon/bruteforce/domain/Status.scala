package com.github.nikdon.bruteforce.domain

import io.circe.{Decoder, Encoder}

object Status {
  case object Ok     extends Status { val name: String = this.productPrefix.toLowerCase }
  case object Failed extends Status { val name: String = this.productPrefix.toLowerCase }

  implicit val statusEncoder: Encoder[Status] = Encoder[String].contramap[Status](_.productPrefix.toLowerCase)
  implicit val statusDecoder: Decoder[Status] = Decoder[String].map {
    case Ok.name     => Ok
    case Failed.name => Failed
  }
}

sealed trait Status extends Product with Serializable { def name: String }
