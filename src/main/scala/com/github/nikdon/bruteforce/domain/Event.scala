package com.github.nikdon.bruteforce.domain

import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec, JsonKey}
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks
import org.apache.flink.streaming.api.watermark.Watermark

@ConfiguredJsonCodec sealed trait Event extends Product with Serializable {
  def creationDate: Long
  def status: Status
}

object Event {
  implicit val config: Configuration = Configuration.default.withSnakeCaseKeys.withDefaults.withDiscriminator("_type")

  @ConfiguredJsonCodec
  final case class Login(@JsonKey("_id") id: String,
                         userName: String,
                         userId: String,
                         sourceIp: String,
                         browser: String,
                         creationDate: Long,
                         status: Status)
      extends Event

  object Login {
    object EventTimestampExtractor extends AssignerWithPeriodicWatermarks[Event.Login] with Serializable {
      override def getCurrentWatermark: Watermark = {
        new Watermark(System.currentTimeMillis() - 1000L)
      }
      override def extractTimestamp(element: Event.Login, previousElementTimestamp: Long): Long = {
        element.creationDate
      }
    }
  }
}
