# Bruteforce Alerting

Bruteforce alerting system on top of Apache Flink.


## Quickstart

### Local mode

1. Start a TCP server, that produces events:

    ```bash
    > nc -l 25501
    ```

2. Start a TCP server, that listen for alerts:

    ```bash
    > nc -l 25502
    ```

3. Assembly a jar via `sbt assembly` and run it with `java -jar bruteforce.jar` or from an IDE with args:

    ```bash
    > sbt assembly
    > java -jar target/scala-2.11/bruteforce.jar --inputHost localhost --inputPort 25501 --outputHost localhost --outputPort 25502 --threshold 3 --windowSize 10 --windowSlide 1 --local true
    ```
    
    where:
    
        * `--inputHost` and `inputPort` - is a TCP address of the event producer
        * `--outputHost` and `outputPort` - is a TCP address of the alert listener
        * `--threshold` - is a threshold for firing alerts
        * `--windowSize` - is a size of window to count failed events in seconds
        * `--windowSlide` - is an advance of the window over time in seconds
        * `--local` - is a mode of execution
    
4. Send events via the server from the step 1

    ```bash
    {"_id":"uuid","user_name":"name","user_id":"user_id","source_ip":"127.0.0.1","browser":"chrome","creation_date":1501482988345,"status":"failed","_type":"Login"}
    ...
    ```
    
    Pay attention to the field `creation_date`. Each incoming event is watermarked with the server's current time minus 
    1 second. For each event system extracts field `creation_date` and if the value is older than watermark 
    it does not considered by the system.
    
5. Look after the alerts

    ```bash
    {"id":"uuid","count":5,"creation_date":1501491726122,"_type":"Login"}
    ...
    {"id":"id","count":4,"creation_date":1501491734239,"_type":"Login"}
    ```
    
## Cluster mode

All steps are the same as for the local mode, but the jar should be submitted to the Flink cluster and 
run with arg `--local false` instead of `--local true`.